package ru.gb;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class PostsPage extends PageView {

    public PostsPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(linkText = "Next Page")
    private WebElement nextPage;

    @FindBy(linkText = "Previous Page")
    private WebElement previousPage;

    @FindBy(xpath = "//h1[contains(text(),'Blog')]")
    private WebElement Blog;

    //@FindBy(xpath = "//a[@href='/posts/4011']/img")
    @FindBy(xpath = "//div[@class='content']/div/a[1]")
    private WebElement firstImg;

    //@FindBy(xpath = "//a[@href='/posts/4011']/h2")
    @FindBy(xpath = "//div[@class='content']/div/a[1]/h2")
    private WebElement firstHeader;

    //@FindBy(xpath = "//a[@href='/posts/4011']/div")
    @FindBy(xpath = "//div[@class='content']/div/a[1]/div")
    private WebElement firstDescript;




    public void assertSuccessPostsPage() {
      //  webDriverWait.until(ExpectedConditions.urlMatches("https://test-stand.gb.ru/"));
        webDriverWait.until(ExpectedConditions.visibilityOf(Blog));
        Assertions.assertEquals("https://test-stand.gb.ru/", driver.getCurrentUrl());
    }
    public PostsPage assertSuccessNewPostsPage() {
        webDriverWait.until(ExpectedConditions.visibilityOf(Blog));
        return new PostsPage(driver);
    }

    public PostsPage assertFirstPostVisible() {
        webDriverWait.until(ExpectedConditions.visibilityOfAllElements(firstImg, firstHeader, firstDescript));
        return new PostsPage(driver);
         }


    public PostsPage clickNextPage() {
        nextPage.click();
        webDriverWait.until(ExpectedConditions.urlContains("https://test-stand.gb.ru/?page"));
        return new PostsPage(driver);
    }

    public PostsPage clickPreviousPage() {
        previousPage.click();
        webDriverWait.until(ExpectedConditions.urlContains("https://test-stand.gb.ru/?page"));
        return new PostsPage(driver);
    }









}