package ru.gb;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class LoginPage extends PageView {



    public LoginPage(WebDriver driver) {
        super(driver);
        PostsPage postsPage = new PostsPage(driver);
    }

    @FindBy(xpath = "//input[@type='text']")
    private WebElement usernameField;

    @FindBy(xpath = "//input[@type='password']")
    private WebElement passwordField;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement loginBtn;

    public PostsPage login(String login, String password) {
        usernameField.sendKeys(login);
        passwordField.sendKeys(password);
        loginBtn.click();
        return new PostsPage(driver);
    }

    public void incorrectLoginAttempt(String login, String password) {
        usernameField.sendKeys(login);
        passwordField.sendKeys(password);
        Assertions.assertFalse(loginBtn.isEnabled());
    }


}
