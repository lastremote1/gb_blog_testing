package ru.gb;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class LoginPageTest {
    static Properties props = new Properties();
    private static InputStream parameters;

    static {
        try {
            parameters = new FileInputStream("src/main/resources/my.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String getMyUsername() throws IOException {
        props.load(parameters);
        return (props.getProperty("username"));

    }
    public String getMyPassword() throws IOException {
        props.load(parameters);
        return (props.getProperty("password"));
    }

    LoginPage loginPage;
    WebDriver driver;

    @BeforeAll
    static void registerDriver() {

        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        options.addArguments("--disable-extensions");
    }

    @BeforeEach
    void initDriver() {
        driver = new ChromeDriver();
        loginPage = new LoginPage(driver);
        driver.navigate().to("https://test-stand.gb.ru/login");
    }
    @Test
    void emptyLoginTest(){
        loginPage.incorrectLoginAttempt(" ", " ");

    }
    @Test
    void badSymbolsLoginTest() {
        loginPage.incorrectLoginAttempt("$#%^ttt", "ssdfg");
    }

    @Test
    void twoSymbolsLoginTest() {
        loginPage.incorrectLoginAttempt("ab", "ba");
    }

    @Test
    void longLoginTest(){
        loginPage.incorrectLoginAttempt("123577997979kfkfff", "sfggrg");
    }

    @Test
    void validLoginTest() throws IOException {
        loginPage.login(getMyUsername(), getMyPassword())
                .assertSuccessPostsPage();
    }



    @AfterEach
    void kill(){
        driver.quit();
    }

}
