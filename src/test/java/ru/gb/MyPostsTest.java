package ru.gb;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.nullValue;

public class MyPostsTest {
    static Properties props = new Properties();
    private static InputStream parameters;

    static {
        try {
            parameters = new FileInputStream("src/main/resources/my.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }



    public String getMyToken() throws IOException {
        props.load(parameters);
        return (props.getProperty("token"));

    }


    @BeforeAll
    static void beforeAll() {
        RestAssured.baseURI = "https://test-stand.gb.ru/api/posts";
        RestAssured.requestSpecification = new RequestSpecBuilder().build();
    }

    @Test
    void ascOrderTest() throws IOException {
        RestAssured.given()
                .header("X-Auth-Token", getMyToken())
                .queryParam("order", "ASC")
                .queryParam("page", "1")
                .log()
                .all()
                .get()
                .then()
                .assertThat()
                .statusCode(200)
                .body("data.id[0]", equalTo(3943))
                .body("data.authorId[0]", equalTo(2007))
                .body("meta.prevPage", equalTo(1))
                .body("meta.nextPage", equalTo(2));

    }
    @Test
    void descOrderTest() throws IOException {
        RestAssured.given()
                .header("X-Auth-Token",getMyToken())
                .queryParam("order", "DESC")
                .queryParam("page", "2")
                .log()
                .all()
                .get()
                .then()
                .assertThat()
                .statusCode(200)
                .body("data.id[0]", equalTo(4009))
                .body("data.authorId[0]", equalTo(2007))
                .body("meta.prevPage", equalTo(1))
                .body("meta.nextPage", equalTo(3));
    }
    @Test
    void descOrderTestNoAuth() {
        RestAssured.given()
                .queryParam("order", "DESC")
                .queryParam("page", "1")
                .log()
                .all()
                .get()
                .then()
                .assertThat()
                .statusCode(401);
    }
    @Test
    void allSortTest() throws IOException {
        RestAssured.given()
                .header("X-Auth-Token",getMyToken())
                .queryParam("order", "ALL")
                .queryParam("page", "1")
                .log()
                .all()
                .get()
                .then()
                .assertThat()
                .statusCode(500);
    }
    @Test
    void nullPageTest() throws IOException {
        RestAssured.given()
                .header("X-Auth-Token",getMyToken())
                .queryParam("page", "100500")
                .log()
                .all()
                .get()
                .then()
                .assertThat()
                .statusCode(200)
                .body("data.id[0]", nullValue())
                .body("data.authorId[0]", nullValue());
    }
}
