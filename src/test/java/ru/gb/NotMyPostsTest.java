package ru.gb;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.hamcrest.CoreMatchers.*;


public class NotMyPostsTest {

    static Properties props = new Properties();
    private static InputStream parameters;

    static {
        try {
            parameters = new FileInputStream("src/main/resources/my.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }



    public String getMyToken() throws IOException {
        props.load(parameters);
        return (props.getProperty("token"));

    }


    @BeforeAll
    static void beforeAll() {
        RestAssured.baseURI = "https://test-stand.gb.ru/api/posts";
        RestAssured.requestSpecification = new RequestSpecBuilder().build();

    }

    @Test
    void ascOrderTest() throws IOException {
        RestAssured.given()
                .header("X-Auth-Token", getMyToken())
                .queryParam("owner", "notMe")
                .queryParam("order", "ASC")
                .queryParam("page", "10")
                .log()
                .all()
                .get()
                .then()
                .assertThat()
                .statusCode(200)
                .body("data.id[0]", notNullValue())
                .body("data.authorId[0]", notNullValue())
                .body("meta.prevPage", equalTo(9))
                .body("meta.nextPage", equalTo(11));

               /* .expect()
                .statusCode(200)
                .response()
                .log()
                .body()
                .when()
                .get();

                */

    }
    @Test
    void descOrderTest() throws IOException {
        RestAssured.given()
                .header("X-Auth-Token",getMyToken())
                .queryParam("owner", "notMe")
                .queryParam("order", "DESC")
                .queryParam("page", "101")
                .log()
                .all()
                .get()
                .then()
                .assertThat()
                .statusCode(200)
                .body("data.id[0]", notNullValue())
                .body("data.authorId[0]", notNullValue())
                .body("meta.prevPage", equalTo(100))
                .body("meta.nextPage", equalTo(102));
    }
    @Test
    void descOrderTestNoAuth() {
        RestAssured.given()
                .queryParam("owner", "notMe")
                .queryParam("order", "DESC")
                .queryParam("page", "101")
                .log()
                .all()
                .get()
                .then()
                .assertThat()
                .statusCode(401);

    }
    @Test
    void allSortTest() throws IOException {
        RestAssured.given()
                .header("X-Auth-Token",getMyToken())
                .queryParam("owner", "notMe")
                .queryParam("order", "ALL")
                .queryParam("page", "101")
                .log()
                .all()
                .get()
                .then()
                .assertThat()
                .statusCode(200);
    }
    @Test
    void nullPageTest() throws IOException {
        RestAssured.given()
                .header("X-Auth-Token",getMyToken())
                .queryParam("owner", "notMe")
                .queryParam("page", "100500")
                .log()
                .all()
                .get()
                .then()
                .assertThat()
                .statusCode(200)
                .body("data.id[0]", nullValue())
                .body("data.authorId[0]", nullValue());

    }


}
