package ru.gb;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PostPageTest {

    static Properties props = new Properties();
    private static InputStream parameters;

    static {
        try {
            parameters = new FileInputStream("src/main/resources/my.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String getMyUsername() throws IOException {
        props.load(parameters);
        return (props.getProperty("username"));

    }
    public String getMyPassword() throws IOException {
        props.load(parameters);
        return (props.getProperty("password"));
    }

    LoginPage loginPage;
    PostsPage postsPage;
    WebDriver driver;

    @BeforeAll
    static void registerDriver() {

        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        options.addArguments("--disable-extensions");


    }

    @BeforeEach
    void initDriver() {
        driver = new ChromeDriver();
        loginPage = new LoginPage(driver);
        postsPage = new PostsPage(driver);
        driver.navigate().to("https://test-stand.gb.ru/login");
    }


    @Test
    void postsPageDisplayedTest() throws IOException {
        loginPage.login(getMyUsername(), getMyPassword())
                .assertSuccessNewPostsPage();
    }

    @Test
    void visibilityOfFirstPostTest() throws IOException {
        loginPage.login(getMyUsername(), getMyPassword())
                .assertSuccessNewPostsPage();
        postsPage.assertFirstPostVisible();
    }

    @Test
    void jumpForwardTest() throws IOException {
        loginPage.login(getMyUsername(), getMyPassword())
                        .assertSuccessNewPostsPage();
        postsPage.clickNextPage()
                .assertSuccessNewPostsPage()
                .assertFirstPostVisible();
    }
    @Test
    void integratedTaskTest() throws IOException {
        loginPage.login(getMyUsername(), getMyPassword())
                .assertSuccessNewPostsPage();
        postsPage.clickNextPage()
                .assertSuccessNewPostsPage()
                .assertFirstPostVisible()
                .clickPreviousPage()
                .assertSuccessNewPostsPage()
                .assertFirstPostVisible();
    }

    @AfterEach
    void kill(){
        driver.quit();
    }

}
