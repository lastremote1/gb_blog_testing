package ru.gb;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LoginLogicTest {
    static Properties props = new Properties();
    private static InputStream parameters;

    static {
        try {
            parameters = new FileInputStream("src/main/resources/my.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String getMyUsername() throws IOException {
        props.load(parameters);
        return (props.getProperty("username"));

    }
    public String getMyPassword() throws IOException {
        props.load(parameters);
        return (props.getProperty("password"));
    }

    @BeforeAll
    static void beforeAll() {
        RestAssured.baseURI = "https://test-stand.gb.ru/gateway/login";
        RestAssured.requestSpecification = new RequestSpecBuilder().build();
    }

    @Test
    void positiveLoginTest() throws IOException {

        RestAssured.given()
                .formParam("username", getMyUsername())
                .formParam("password", getMyPassword())
                .log()
                .all()
                .expect()
                .statusCode(200)
                .log()
                .body()
                .when()
                .post();

    }
    @Test
    void invalidCredsTest(){

        RestAssured.given()
                .formParam("username", "nemezida")
                .formParam("password", "qwerty")
                .log()
                .all()
                .expect()
                .statusCode(401)
                .log()
                .body()
                .when()
                .post();
    }
    @Test
    void invalidParamsTest(){

        RestAssured.given()
                .log()
                .all()
                .expect()
                .statusCode(400)
                .log()
                .body()
                .when()
                .post();
    }


}
